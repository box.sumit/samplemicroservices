from flask import Flask
import requests
import os

app = Flask(__name__)

@app.route('/details')
def home():
    return 'Hello from Microservice 2 - details!'

@app.route('/details/ratings')
def ratings():
    ratings_domain = "ratings:9080" if (os.environ.get("RATINGS_DOMAIN") is None) else os.environ.get("RATINGS_DOMAIN")

    response = requests.get('http://{0}/ratings'.format(ratings_domain))
    response.raise_for_status()
    text = response.text

    return text

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9080)

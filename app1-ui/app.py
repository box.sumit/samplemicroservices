from flask import Flask
import requests
import os

app = Flask(__name__)

@app.route('/')
def home():
    details_domain = "details:9080" if (os.environ.get("DETAILS_DOMAIN") is None) else os.environ.get("DETAILS_DOMAIN")
    review_domain = "reviews:9080" if (os.environ.get("REVIEWS_DOMAIN") is None) else os.environ.get("REVIEWS_DOMAIN")

    with_microservice = "Connect with Microservice" if details_domain == "details:9080" else "Connect with Gloo gateway"

    details = requests.get('http://{0}/details'.format(details_domain))
    details.raise_for_status()
    
    reviews = requests.get('http://{0}/reviews'.format(review_domain))
    reviews.raise_for_status()
     
    results = {
        "details": details.text,
        "reviews": reviews.text,
    }

    return {
        "ui": "Welcome to UI",
        "results": results,
        "status": with_microservice
    }

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9080)
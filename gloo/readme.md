
# Gloo Installation

    Mesh

    meshctl install --profiles gloo-mesh-single,ratelimit,extauth \
        --set common.cluster=$CLUSTER_NAME \
        --set demo.manageAddonNamespace=true \
        --set istioInstallations.enabled=false \
        --version $GLOO_VERSION \
        --set licensing.glooMeshLicenseKey=$GLOO_MESH_LICENSE_KEY \

    
    Gateway

    meshctl install --profiles gloo-gateway-demo \
        --set common.cluster=$CLUSTER_NAME \
        --set licensing.glooGatewayLicenseKey=$GLOO_GATEWAY_LICENSE_KEY \
        --set licensing.glooNetworkLicenseKey=$GLOO_NETWORK_LICENSE_KEY


# Trail Run

    kubectl create namespace freniz
    kubectl label ns freniz istio.io/rev=$REVISION --overwrite=true

    Step 1:
        kubectl -n freniz apply -f config-map.yaml
        kubectl -n freniz apply -f kubectl-config.yaml

    Step 2:
        kubectl -n freniz apply -f virtual-gateway.yaml

    Step 3:
        kubectl -n freniz apply -f routetable.yaml




# Modify 

    RATINGS_DOMAIN: 10.98.210.241

    kubectl -n freniz config-map.yaml

    Restart Deployment
        kubectl -n freniz rollout restart deployment reviews     


# Delete

    kubectl delete ConfigMap my-configmap -n freniz
from flask import Flask

app = Flask(__name__)

@app.route('/ratings')
def home():
    return 'Hello from Microservice 4 - ratings!'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9080)

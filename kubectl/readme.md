# Sample Microservices


Step 1:

docker build -t abdulnizam/microservice1 .
docker build -t abdulnizam/microservice2 .



Step 2:

kubectl apply -f microservice1-deployment.yaml
kubectl apply -f microservice1-service.yaml
kubectl apply -f microservice2-deployment.yaml
kubectl apply -f microservice2-service.yaml


Step 3: 

To access in the browser, we need to expose the port 

kubectl port-forward service/microservice1 8080:6000


abdulnizam/microservice1:V1.0 - http call
abdulnizam/microservice1:V2.0 - https call

